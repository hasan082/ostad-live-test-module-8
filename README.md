# Contact List App

This is a Flutter app that displays a list of contacts and allows users to view their details.

## Screenshots

<img src="https://gitlab.com/hasan082/ostad-live-test-module-8/-/raw/master/list.png" alt="" width="300">
<img src="https://gitlab.com/hasan082/ostad-live-test-module-8/-/raw/master/bottomsheet2.png" alt=""  width="300">

## Structure

The app follows a modular structure with separate files for the controller, pages, models, and the main entry point.

- `main.dart`: The main entry point of the app.
- `contact.dart`: Defines the `Contact` model class.
- `contact_list_controller.dart`: Implements the `ContactListController` class responsible for managing the list of contacts.
- `contact_list_page.dart`: Represents the page that displays the contact list and handles interactions.
- Other supporting files: Other necessary files like `pubspec.yaml`, etc.

## Getting Started

To run the app, follow these steps:

1. Ensure you have Flutter and Dart SDK installed.
2. Clone the repository.
3. Open the project in your preferred IDE or text editor.
4. Run `flutter pub get` to install the dependencies.
5. Connect a device or start an emulator.
6. Run `flutter run` to launch the app on the device/emulator.

## Customization

You can customize the app by modifying the following files:

- To add more contacts, open `contact_list_controller.dart` and update the `contacts` list in the `ContactListController` class.
- To customize the UI of the contact list page or the bottom sheet, open `contact_list_page.dart` and modify the relevant widgets.

Feel free to explore and extend the functionality of the app as per your requirements.

## License

This project is licensed under the [MIT License](https://opensource.org/license/mit/).

import 'package:flutter/material.dart';
import '../models/contacts_modles.dart';

class ContactListController {

  List<Contact> contacts = [
  Contact(name: 'John Doe', email: 'john.doe@example.com', phoneNumber: '123-456-7890'),
  Contact(name: 'Jane Smith', email: 'jane.smith@example.com', phoneNumber: '987-654-3210'),
  Contact(name: 'Mike Johnson', email: 'mike.johnson@example.com', phoneNumber: '555-123-4567'),
  Contact(name: 'Sarah Adams', email: 'sarah.adams@example.com', phoneNumber: '111-222-3333'),
  Contact(name: 'David Brown', email: 'david.brown@example.com', phoneNumber: '444-555-6666'),
  Contact(name: 'Emma Clark', email: 'emma.clark@example.com', phoneNumber: '777-888-9999'),
  Contact(name: 'Ryan Davis', email: 'ryan.davis@example.com', phoneNumber: '101-202-3030'),
  Contact(name: 'Olivia Evans', email: 'olivia.evans@example.com', phoneNumber: '404-505-6060'),
  Contact(name: 'William Foster', email: 'william.foster@example.com', phoneNumber: '707-808-9090'),
  Contact(name: 'Sophia Gray', email: 'sophia.gray@example.com', phoneNumber: '123-456-7890'),
  ];



  void showContactDetails(BuildContext context, Contact contact) {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return FractionallySizedBox(
          widthFactor: 1, // Adjust the width as needed
          child: Container(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  contact.name,
                  style: const TextStyle(
                      fontSize: 20, fontWeight: FontWeight.bold),
                ),
                const SizedBox(height: 8),
                Text(
                  'Email: ${contact.email}',
                  style: const TextStyle(
                      fontSize: 16, fontWeight: FontWeight.w400),
                ),
                const SizedBox(height: 8),
                Text(
                  'Phone: ${contact.phoneNumber}',
                  style: const TextStyle(
                      fontSize: 16, fontWeight: FontWeight.w400),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}

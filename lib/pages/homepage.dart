import 'package:flutter/material.dart';
import 'package:ostad_live_test_module_7/controller/contact_controller.dart';


class ContactListScreen extends StatefulWidget {
  const ContactListScreen({super.key,});

  @override
  State<ContactListScreen> createState() => _ContactListScreenState();
}

class _ContactListScreenState extends State<ContactListScreen> {

  ContactListController controller = ContactListController();

 
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xFFF5EEFA),
        title: const Text('Contact List'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(12.0),
        child: ListView.builder(
          itemCount: controller.contacts.length,
          itemBuilder: (BuildContext context, int index) {
            final contact = controller.contacts[index];
            return Card(
              margin: const EdgeInsets.only(top: 12),
              elevation: 2,
              child: ListTile(
                title: Text(contact.name),
                subtitle: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(contact.email),
                    const SizedBox(height: 5,),
                    Text(contact.phoneNumber),
                  ],
                ),
                onTap: () => controller.showContactDetails(context, contact),
              ),
            );
          },
        ),
      ),
    );
  }
}